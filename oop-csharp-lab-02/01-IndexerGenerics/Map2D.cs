﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;
        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach (TKey1 k1 in keys1)
            {
                foreach (TKey2 k2 in keys2)
                {
                    values.Add(Tuple.Create(k1, k2), generator.Invoke(k1, k2));
                    //Console.Write(values[Tuple.Create(k1, k2)] + " ");
                }
            }

        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            return other.Equals(values) ? true : false; 
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
               TValue tmp;
               values.TryGetValue(Tuple.Create(key1, key2), out tmp);
                return tmp;
            }

            set
            {
               
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> row = new List<Tuple<TKey2, TValue>>();
            ICollection<Tuple<TKey1, TKey2>> coll = values.Keys;

                foreach (Tuple<TKey1, TKey2> t in coll)
                {
                    if (t.Item1.Equals(key1))
                    {
                        TValue tmp;
                        values.TryGetValue(t, out tmp);
                        row.Add(Tuple.Create(t.Item2, tmp));
                    }
                }
            


            return row;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> column = new List<Tuple<TKey1, TValue>>();
            ICollection<Tuple<TKey1, TKey2>> coll = values.Keys;
            
                foreach (Tuple<TKey1, TKey2> t in coll)
                {
                    if (t.Item2.Equals(key2))
                    {
                        TValue tmp;
                        values.TryGetValue(t, out tmp);
                        column.Add(Tuple.Create(t.Item1, tmp));
                    }
                }
            


            return column;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> elements = new List<Tuple<TKey1, TKey2, TValue>>();
            ICollection<Tuple<TKey1, TKey2>> coll = values.Keys;
            
                foreach (Tuple<TKey1, TKey2> t in coll)
                {
                    TValue tmp;
                    values.TryGetValue(t, out tmp);
                    elements.Add(Tuple.Create(t.Item1, t.Item2, tmp));
                }
            


            return elements;
        }

        public int NumberOfElements
        {
            get
            {
                return values.Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
